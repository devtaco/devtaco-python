# 문자열 슬라이싱
print('Python'[::-1])

# 문자열 슬라이싱을 이용해 문자열의 일부를 복사할 수 있다.
p = 'Python'
print(p[0:2])

print(p[:2])

print(p[-2:]) # 뒷부분 (역순)

print(p[:]) # 전체

print(p[::-1]) # 역순으로 쭉

# 문자열의 lower() method  -> 소문자로
print(p.lower())

# 문자열의 replace() -> 일부 문자열을 바꾼다

print(p.replace('P', 'J')) # Python -> Jython


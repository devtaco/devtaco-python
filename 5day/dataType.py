# type()
# 수치형
print(type(6)) # class 'int' 정수

print(type(2.8)) # class 'float' 부동소수

print(type(3+4j)) # class 'complex' 복소수

# 순서형 -> for 문에서 활용 가능한 것들
print(type('A')) # class 'str'

print(type(['love','enemy','fault'])) # class 'list'

print(type(('love','enemy','fault'))) # class 'tuple'

# 매핑형 -> dict 은 key, value의 짝으로 이루어진 mapping형

print(type({'one': 1, 'two':2, 'three' : 3 })) # class 'dict'

# 불린형 -> true / false

print(type(3 >= 1)) # class 'boolean'

# 세트형 (집합, 순서X) 

fruits = { 'apple', 'banana', 'orange' }
print(type(fruits)) # class 'set'

# 주어진 단어가 회문인지 판별하는 함수 palindrome()을 작성하세요.
# 단, 문자열 입력은 모두 소문자로 이뤄지며 공백을 포함하지 않는다고 가정합니다.
# 회문 : 앞에서부터읽어도, 뒤에서부터 읽어도 같은 단어

def palindrome_1(param):
  p = param
  if ( p[::-1] == p ) :
    return True
  else :
    return False

def palindrome_2(param):
  p = param.lower()
  if ( p[::-1] == p ) :
    return True
  else :
    return False

def palindrome_3(param):
  p = param.lower().replace(' ', '')
  if ( p[::-1] == p ) :
    return True
  else :
    return False

print( palindrome_1('anna') )
print( palindrome_1('banana') )
print( palindrome_2('Anna') )
print( palindrome_3('My gym'))